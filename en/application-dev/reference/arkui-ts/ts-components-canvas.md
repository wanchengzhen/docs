# Canvas Components<a name="EN-US_TOPIC_0000001192595180"></a>

-   **[Canvas](ts-components-canvas-canvas.md)**  

-   **[CanvasRenderingContext2D](ts-canvasrenderingcontext2d.md)**  

-   **[OffscreenCanvasRenderingConxt2D](ts-offscreencanvasrenderingcontext2d.md)**  

-   **[Lottie](ts-components-canvas-lottie.md)**  

-   **[Path2D](ts-components-canvas-path2d.md)**  

-   **[CanvasGradient](ts-components-canvas-canvasgradient.md)**  

-   **[ImageBitmap](ts-components-canvas-imagebitmap.md)**  

-   **[ImageData](ts-components-canvas-imagedata.md)**  


