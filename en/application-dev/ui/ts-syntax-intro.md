# Overview<a name="EN-US_TOPIC_0000001110948892"></a>

This section defines the core mechanism and functions of the TypeScript-based declarative development paradigm. It acquaints you with the declarative UI descriptions, componentization mechanisms, UI state management, rendering control syntax, and syntactic sugar.

Follow the provided guidelines for UI development. For details about the components, see  [Components](../reference/arkui-js/js-components.md).

>![](../public_sys-resources/icon-note.gif) **NOTE:** 
>-   All examples use the TypeScript \(TS\) language. If you are using another language, comply with the syntax requirements for that language.
>-   The components used in the examples are preset in the UI framework and are used only to explain the UI description specifications.
>-   Universal attribute and event methods generally apply to all components, and the attribute and event methods within a component apply only to this component.

