# Hi3861开发板<a name="ZH-CN_TOPIC_0000001171774078"></a>

-   **[安装开发板环境](quickstart-lite-steps-hi3861-setting.md)**  

-   **[新建应用程序](quickstart-lite-steps-hi3861-application-framework.md)**  

-   **[编译](quickstart-lite-steps-hi3861-building.md)**  

-   **[烧录](quickstart-lite-steps-hi3861-burn.md)**  

-   **[调试验证](quickstart-lite-steps-hi3861-debug.md)**  

-   **[运行](quickstart-lite-steps-hi3861-running.md)**  

-   **[常见问题](quickstart-lite-steps-hi3861-faqs.md)**  


