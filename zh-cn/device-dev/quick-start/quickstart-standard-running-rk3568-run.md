# 镜像运行<a name="ZH-CN_TOPIC_0000001233927469"></a>

-   [启动系统](#section646361191511)
-   [运行“Hello World”](#section11845976150)

## 启动系统<a name="section646361191511"></a>

镜像烧录完成并连接电源线之后，系统将会自动启动。开发板附带的屏幕呈现以下界面，表明系统已运行成功。

**图 1**  系统启动效果图<a name="fig1642274764110"></a>  
![](figures/系统启动效果图.jpg "系统启动效果图")

## 运行“Hello World”<a name="section11845976150"></a>

-   设备启动后打开串口工具（以putty为例），波特率设置为1500000，连接设备。

    ![](figures/rk3568-run-configuration.png)

-   打开串口后，在任意目录（以设备根目录为例）下输入命令helloworld后回车，即出现“Hello World!”字样。

    ![](figures/rk3568-helloworld.png)


