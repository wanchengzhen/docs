# CSS Animation<a name="EN-US_TOPIC_0000001211008759"></a>

-   **[Defining Attribute Style Animations](ui-js-animate-attribute-style.md)**  

-   **[Defining Animations with the transform Attribute](ui-js-animate-transform.md)**  

-   **[Defining Animations with the background-position Attribute](ui-js-animate-background-position-style.md)**  


