# 声明式UI描述规范

- **[无参数构造配置](ts-parameterless-configuration.md)**

- **[必选参数构造配置](ts-configuration-with-mandatory-parameters.md)**

- **[属性配置](ts-attribution-configuration.md)**

- **[事件配置](ts-event-configuration.md)**

- **[子组件配置](ts-child-component-configuration.md)**