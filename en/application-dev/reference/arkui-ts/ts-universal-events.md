# Universal Events<a name="EN-US_TOPIC_0000001237715071"></a>

-   **[Click Event](ts-universal-events-click.md)**  

-   **[Touch](ts-universal-events-touch.md)**  

-   **[Show/Hide Event](ts-universal-events-show-hide.md)**  

-   **[Key Event](ts-universal-events-key.md)**  

-   **[Component Area Change Event](ts-universal-events-component-area-change.md)**  


