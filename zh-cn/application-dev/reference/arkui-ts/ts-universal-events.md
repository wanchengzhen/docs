# 通用事件

- **[点击事件](ts-universal-events-click.md)**

- **[触摸事件](ts-universal-events-touch.md)**

- **[挂载卸载事件](ts-universal-events-show-hide.md)**

- **[拖拽事件](ts-universal-events-drag-drop.md)**

- **[按键事件](ts-universal-events-key.md)**

- **[焦点事件](ts-universal-focus-event.md)**

- **[鼠标事件](ts-universal-mouse-key.md)**

- **[组件区域变化事件](ts-universal-component-area-change-event.md)**

