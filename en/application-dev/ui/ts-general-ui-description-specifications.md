# General UI Description Specifications<a name="EN-US_TOPIC_0000001157388843"></a>

-   **[Basic Concepts](ts-general-ui-concepts.md)**  

-   **[Declarative UI Description Specifications](ts-declarative-ui-description-specifications.md)**  

-   **[Componentization](ts-component-based.md)**  


