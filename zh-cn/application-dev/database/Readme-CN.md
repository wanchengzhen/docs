# 数据管理

- 分布式数据服务
  - [分布式数据服务概述](database-mdds-overview.md)
  - [分布式数据服务开发指导](database-mdds-guidelines.md)
- 关系型数据库
  - [关系型数据库概述](database-relational-overview.md)
  - [分布式数据服务开发指导](database-relational-guidelines.md)
- 轻量级数据存储
  - [轻量级数据存储概述](database-preference-overview.md)
  - [轻量级数据存储开发指导](database-preference-guidelines.md)
- 分布式数据对象
  - [分布式数据对象概述](database-distributedobject-overview.md)
  - [分布式数据对象开发指导](database-distributedobject-guidelines.md)
