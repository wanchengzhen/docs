# tab-content<a name="ZH-CN_TOPIC_0000001173164727"></a>

<[tabs](js-components-container-tabs.md)\>的子组件，用来展示tab的内容区，高度默认充满tabs剩余空间，子组件排列方式为横向排列，当作为容器组件的子元素时在主轴方向需要设置tab-content的确定长度，否则无法显示。

## 权限列表<a name="section11257113618419"></a>

无

## 子组件<a name="s6831289f2fac48a892746bd57f336c26"></a>

支持。

## 属性<a name="s623ab9c704b24c329d0e91e85a30263e"></a>

除支持[通用属性](js-components-common-attributes.md)外，还支持如下属性：

<table><thead align="left"><tr><th class="cellrowborder" valign="top" width="23.119999999999997%" id="mcps1.1.6.1.1"><p>名称</p>
</th>
<th class="cellrowborder" valign="top" width="23.119999999999997%" id="mcps1.1.6.1.2"><p>类型</p>
</th>
<th class="cellrowborder" valign="top" width="10.48%" id="mcps1.1.6.1.3"><p>默认值</p>
</th>
<th class="cellrowborder" valign="top" width="7.5200000000000005%" id="mcps1.1.6.1.4"><p>必填</p>
</th>
<th class="cellrowborder" valign="top" width="35.76%" id="mcps1.1.6.1.5"><p>描述</p>
</th>
</tr>
</thead>
<tbody><tr><td class="cellrowborder" valign="top" width="23.119999999999997%" headers="mcps1.1.6.1.1 "><p>scrollable</p>
</td>
<td class="cellrowborder" valign="top" width="23.119999999999997%" headers="mcps1.1.6.1.2 "><p>boolean</p>
</td>
<td class="cellrowborder" valign="top" width="10.48%" headers="mcps1.1.6.1.3 "><p>true</p>
</td>
<td class="cellrowborder" valign="top" width="7.5200000000000005%" headers="mcps1.1.6.1.4 "><p>否</p>
</td>
<td class="cellrowborder" valign="top" width="35.76%" headers="mcps1.1.6.1.5 "><p>是否可以通过左右滑动进行页面切换。默认为true，设置为false后，页面的切换只能通过tab-bar的点击实现。</p>
</td>
</tr>
</tbody>
</table>

## 样式<a name="section15751323144115"></a>

支持[通用样式](js-components-common-styles.md)。

## 事件<a name="s0c39d3f35b9d4af69a19456ae743c675"></a>

支持[通用事件](js-components-common-events.md)。

## 示例<a name="section11929165914411"></a>

详见[tabs示例](js-components-container-tabs.md#section14993155318710)。

