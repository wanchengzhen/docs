# 手势处理

- **[组合手势](ts-combined-gestures.md)**

- **[绑定手势方法](ts-gesture-settings.md)**

- **[基础手势](ts-basic-gestures.md)**