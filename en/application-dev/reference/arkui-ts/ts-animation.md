# Animation<a name="EN-US_TOPIC_0000001237355053"></a>

-   **[Attribute Animation](ts-animatorproperty.md)**  

-   **[Explicit Animation](ts-explicit-animation.md)**  

-   **[Transition Animation](ts-transition-animation.md)**  

-   **[Motion Path Animation](ts-motion-path-animation.md)**  

-   **[Matrix Transformation](ts-matrix-transformation.md)**  

-   **[Interpolation Calculation](ts-interpolation-calculation.md)**  


