# Gesture Processing<a name="EN-US_TOPIC_0000001192595144"></a>

-   **[Gesture Binding Methods](ts-gesture-settings.md)**  

-   **[Basic Gestures](ts-basic-gestures.md)**  

-   **[Combined Gestures](ts-combined-gestures.md)**  


