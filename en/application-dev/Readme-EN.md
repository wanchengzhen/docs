# Application Development

-   [Application Development Overview](application-dev-guide.md)
-   [DevEco Studio \(OpenHarmony\) User Guide](quick-start/deveco-studio-user-guide-for-openharmony.md)
-   [Directory Structure](quick-start/package-structure.md)
-   [Getting Started](quick-start/start.md)
-   [ArkUI](ui/ui-arkui.md)
    -   [JavaScript-based Web-like Development Paradigm](ui/ui-arkui-js.md)

    -   [TypeScript-based Declarative Development Paradigm](ui/ui-arkui-ts.md)
-   [Agent-Powered Scheduled Reminders ](background-agent-scheduled-reminder/Readme-EN.md)
-   [Background Task Management ](background-task-management/Readme-EN.md)
-   [Media](media/Readme-EN.md)
-   [Security](security/Readme-EN.md)
-   [Connectivity](connectivity/Readme-EN.md)
-   [Data Management](database/Readme-EN.md)
-   [USB Service](usb/Readme-EN.md)
-   [DFX](dfx/Readme-EN.md)
-   [Development References](reference/Readme-EN.md)
    -   [JavaScript-based Web-like Development Paradigm](reference/arkui-js/Readme-EN.md)
    -   [TypeScript-based Declarative Development Paradigm](reference/arkui-ts/Readme-EN.md)
    -   [APIs](reference/apis/Readme-EN.md)
