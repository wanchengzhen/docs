# 通用UI描述规范

- **[基本概念](ts-general-ui-concepts.md)**

- **[声明式UI描述规范](ts-declarative-ui-description-specifications.md)**

- **[组件化](ts-component-based.md)**