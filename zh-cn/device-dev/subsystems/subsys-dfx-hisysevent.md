# HiSysEvent开发指导<a name="ZH-CN_TOPIC_0000001195021448"></a>

-   **[HiSysEvent打点配置指导](subsys-dfx-hisysevent-logging-config.md)**

-   **[HiSysEvent打点指导](subsys-dfx-hisysevent-logging.md)**

-   **[HiSysEvent订阅指导](subsys-dfx-hisysevent-listening.md)**

-   **[HiSysEvent查询指导](subsys-dfx-hisysevent-querying.md)**

-   **[HiSysEvent工具使用指导](subsys-dfx-hisysevent-tool.md)**