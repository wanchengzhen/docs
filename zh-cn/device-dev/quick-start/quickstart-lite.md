# 轻量和小型系统入门<a name="ZH-CN_TOPIC_0000001112826850"></a>

-   **[轻量与小型系统入门概述](quickstart-lite-overview.md)**  

-   **[搭建轻量与小型系统环境](quickstart-lite-env-setup.md)**  

-   **[运行“Hello World”](quickstart-lite-steps.md)**  

-   **[附录](quickstart-lite-introduction.md)**  


