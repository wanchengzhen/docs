# 全局UI方法

- **[图片缓存](ts-methods-image-cache.md)**

- **[媒体查询](ts-methods-media-query.md)**

- **[弹窗](ts-methods-popup-window.md)**