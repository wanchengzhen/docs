# Global UI Methods<a name="EN-US_TOPIC_0000001237475047"></a>

-   **[Alert Dialog Box](ts-methods-alert-dialog-box.md)**  

-   **[Custom Dialog box](ts-methods-custom-dialog-box.md)**  

-   **[Image Cache](ts-methods-image-cache.md)**  

-   **[Media Query](ts-methods-media-query.md)**  

-   **[List Selection Dialog Box](ts-methods-custom-actionsheet.md)**  


