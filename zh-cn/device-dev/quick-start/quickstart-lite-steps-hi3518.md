# Hi3518开发板<a name="ZH-CN_TOPIC_0000001216535385"></a>

-   **[安装开发板环境](quickstart-lite-steps-hi3518-setting.md)**  

-   **[新建应用程序](quickstart-lite-steps-hi3518-application-framework.md)**  

-   **[编译](quickstart-lite-steps-hi3518-building.md)**  

-   **[烧录](quickstart-lite-steps-hi3518-burn.md)**  

-   **[运行](quickstart-lite-steps-hi3518-running.md)**  

-   **[常见问题](quickstart-lite-steps-hi3518-faqs.md)**  


