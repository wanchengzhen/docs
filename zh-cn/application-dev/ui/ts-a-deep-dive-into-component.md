# 深入理解组件化

- **[build函数](ts-function-build.md)**

- **[自定义组件成员变量初始化](ts-custom-component-initialization.md)**

- **[自定义组件生命周期回调函数](ts-custom-component-lifecycle-callbacks.md)**

- **[组件创建和重新初始化](ts-component-creation-re-initialization.md)**