# 基础组件

- **[Blank](ts-basic-components-blank.md)**

- **[Button](ts-basic-components-button.md)**

- **[DataPanel](ts-basic-components-datapanel.md)**

- **[DatePicker](ts-basic-components-datepicker.md)**

- **[Divider](ts-basic-components-divider.md)**

- **[Image](ts-basic-components-image.md)**

- **[ImageAnimator](ts-basic-components-imageanimator.md)**

- **[Progress](ts-basic-components-progress.md)**

- **[QRCode](ts-basic-components-qrcode.md)**

- **[Rating](ts-basic-components-rating.md)**

- **[Span](ts-basic-components-span.md)**

- **[Slider](ts-basic-components-slider.md)**

- **[Text](ts-basic-components-text.md)**

- **[TextArea](ts-basic-components-textarea.md)**

- **[TextInput](ts-basic-components-textinput.md)**

- **[TextPicker](ts-basic-components-textpicker.md)**

- **[TextTimer](ts-basic-components-texttimer.md)**

- **[Toggle](ts-basic-components-toggle.md)**

- **[Select](ts-basic-components-select.md)**
- **[TextClock](ts-basic-components-textClock.md)**

