# Version Change History<a name="EN-US_TOPIC_0000001210143219"></a>

-   [V3.0 Beta2 \(2021-12-31\)](#section18825185716537)
    -   [Version Compatibility](#section8155205312218)
    -   [Version Change History](#section1655415918226)

-   [V3.0 Beta1 \(2021-09-29\)](#section21092033115018)

## V3.0 Beta2 \(2021-12-31\)<a name="section18825185716537"></a>

### Version Compatibility<a name="section8155205312218"></a>

DevEco Studio 3.0 Beta2 is compatible with the module versions listed below.

<a name="table912419211138"></a>
<table><thead align="left"><tr id="row141241921231"><th class="cellrowborder" valign="top" width="31.009999999999998%" id="mcps1.1.4.1.1"><p id="p1112432111316"><a name="p1112432111316"></a><a name="p1112432111316"></a>Module</p>
</th>
<th class="cellrowborder" valign="top" width="36.02%" id="mcps1.1.4.1.2"><p id="p111241921039"><a name="p111241921039"></a><a name="p111241921039"></a>Version</p>
</th>
<th class="cellrowborder" valign="top" width="32.97%" id="mcps1.1.4.1.3"><p id="p863410439478"><a name="p863410439478"></a><a name="p863410439478"></a>Description</p>
</th>
</tr>
</thead>
<tbody><tr id="row14124221933"><td class="cellrowborder" valign="top" width="31.009999999999998%" headers="mcps1.1.4.1.1 "><p id="p17124172110319"><a name="p17124172110319"></a><a name="p17124172110319"></a>Gradle</p>
</td>
<td class="cellrowborder" valign="top" width="36.02%" headers="mcps1.1.4.1.2 "><p id="p101251821838"><a name="p101251821838"></a><a name="p101251821838"></a>7.3 (7.2 at minimum)</p>
</td>
<td class="cellrowborder" valign="top" width="32.97%" headers="mcps1.1.4.1.3 "><p id="p0634643124719"><a name="p0634643124719"></a><a name="p0634643124719"></a>DevEco Studio has Gradle 7.3 preinstalled. No separate installation is required.</p>
</td>
</tr>
<tr id="row1125172118320"><td class="cellrowborder" valign="top" width="31.009999999999998%" headers="mcps1.1.4.1.1 "><p id="p358213219512"><a name="p358213219512"></a><a name="p358213219512"></a>JDK</p>
</td>
<td class="cellrowborder" valign="top" width="36.02%" headers="mcps1.1.4.1.2 "><p id="p1412552119316"><a name="p1412552119316"></a><a name="p1412552119316"></a>11.0.x</p>
</td>
<td class="cellrowborder" valign="top" width="32.97%" headers="mcps1.1.4.1.3 "><p id="p76347438478"><a name="p76347438478"></a><a name="p76347438478"></a>DevEco Studio has JDK 11 preinstalled. No separate installation is required.</p>
</td>
</tr>
<tr id="row712518211231"><td class="cellrowborder" valign="top" width="31.009999999999998%" headers="mcps1.1.4.1.1 "><p id="p61257211232"><a name="p61257211232"></a><a name="p61257211232"></a>OpenHarmony SDK</p>
</td>
<td class="cellrowborder" valign="top" width="36.02%" headers="mcps1.1.4.1.2 "><p id="p141258211631"><a name="p141258211631"></a><a name="p141258211631"></a>3.1.0.0 (API Version 8 Beta)</p>
</td>
<td class="cellrowborder" valign="top" width="32.97%" headers="mcps1.1.4.1.3 "><p id="p363464312479"><a name="p363464312479"></a><a name="p363464312479"></a>This version is compatible with SDKs of earlier versions.</p>
</td>
</tr>
<tr id="row148771316264"><td class="cellrowborder" valign="top" width="31.009999999999998%" headers="mcps1.1.4.1.1 "><p id="p16276131516263"><a name="p16276131516263"></a><a name="p16276131516263"></a>Toolchinas</p>
</td>
<td class="cellrowborder" valign="top" width="36.02%" headers="mcps1.1.4.1.2 "><p id="p887713152611"><a name="p887713152611"></a><a name="p887713152611"></a>3.1.0.0</p>
</td>
<td class="cellrowborder" rowspan="3" valign="top" width="32.97%" headers="mcps1.1.4.1.3 "><p id="p14933418194918"><a name="p14933418194918"></a><a name="p14933418194918"></a>Update them to the latest version.</p>
</td>
</tr>
<tr id="row1792520182520"><td class="cellrowborder" valign="top" headers="mcps1.1.4.1.1 "><p id="p109251518258"><a name="p109251518258"></a><a name="p109251518258"></a>hap plug-in</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.1.4.1.2 "><p id="p14925111817513"><a name="p14925111817513"></a><a name="p14925111817513"></a>3.0.5.2</p>
</td>
</tr>
<tr id="row29251718254"><td class="cellrowborder" valign="top" headers="mcps1.1.4.1.1 "><p id="p109255186518"><a name="p109255186518"></a><a name="p109255186518"></a>decctest plug-in</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.1.4.1.2 "><p id="p15925121819513"><a name="p15925121819513"></a><a name="p15925121819513"></a>1.2.7.2</p>
</td>
</tr>
</tbody>
</table>

### Version Change History<a name="section1655415918226"></a>

<a name="simpletable154972061571"></a>
<table id="simpletable154972061571"><tr id="strow04971366576"><td valign="top" id="stentry164971967578"><p id="p949718615570"><a name="p949718615570"></a><a name="p949718615570"></a><strong id="b3206161751212"><a name="b3206161751212"></a><a name="b3206161751212"></a>New Features</strong></p>
<a name="ul1978261655712"></a><a name="ul1978261655712"></a><ul id="ul1978261655712"><li>Introduced the Chinese UI. By default, the UI is displayed in English. To enable the Chinese UI, go to <strong id="b121121320121217"><a name="b121121320121217"></a><a name="b121121320121217"></a>Settings</strong>, choose <strong id="b7112102015129"><a name="b7112102015129"></a><a name="b7112102015129"></a>Plugins</strong> &gt; <strong id="b5112152017128"><a name="b5112152017128"></a><a name="b5112152017128"></a>installed</strong>, and select <strong id="b9113182071210"><a name="b9113182071210"></a><a name="b9113182071210"></a>Chinese (Simplified)</strong>. Then, restart DevEco Studio for the settings to take effect.</li><li>Introduced support for OpenHarmony apps and services, with various debugging features, from breakpoint management and variable observation to stepping.</li></ul>
<p id="p49051145125817"><a name="p49051145125817"></a><a name="p49051145125817"></a></p>
<p id="p7511347135817"><a name="p7511347135817"></a><a name="p7511347135817"></a><strong id="b6867529171215"><a name="b6867529171215"></a><a name="b6867529171215"></a>Enhanced Features</strong></p>
<a name="ul311913348114"></a><a name="ul311913348114"></a><ul id="ul311913348114"><li>Updated the OpenHarmony SDK to 3.1.0.0, whose API version is API 8 Beta and corresponding compilation and building plugin is 3.0.5.2.</li><li>Added an ability template that supports low-code development: <strong id="b736929122019"><a name="b736929122019"></a><a name="b736929122019"></a>[Standard]Empty Ability</strong>.</li><li>Added eTS component preview: allows previewing of eTS components; requires compileSdkVersion 8 or later.</li><li>Added eTS livee preview: allows viewing of the attribute changes in real time as you make them; requires compileSdkVersion 8 or later.</li></ul>
</td>
</tr>
</table>

## V3.0 Beta1 \(2021-09-29\)<a name="section21092033115018"></a>

<a name="simpletable19435134375015"></a>
<table id="simpletable19435134375015"><tr id="strow1435543185020"><td valign="top" id="stentry64351943115013"><div class="p" id="p13974162220455"><a name="p13974162220455"></a><a name="p13974162220455"></a><strong id="b0838112318282"><a name="b0838112318282"></a><a name="b0838112318282"></a>New Features</strong><a name="ul11381034104515"></a><a name="ul11381034104515"></a><ul id="ul11381034104515"><li>Added support for OpenHarmony SDK management. You can use SDK Manager to download and manage OpenHarmony SDKs.</li><li>Allowed for building of a single module during HAP compilation and building to accelerate building for multi-module projects; allowed for one-click re-building of HAPs, by automatically conducting the Clean Project operation before a HAP build.</li></ul>
</div>
<div class="p" id="p556811306614"><a name="p556811306614"></a><a name="p556811306614"></a><strong id="b0183135142820"><a name="b0183135142820"></a><a name="b0183135142820"></a>Enhanced Features</strong><a name="ul834518400613"></a><a name="ul834518400613"></a><ul id="ul834518400613"><li>Updated the compilation and building plugin to version 3.0.3.2.</li><li>Improved the JSON editor, which now enables quick rectification of resource index errors and instant access to resource values.</li><li>Provided Ohos and Project (default) views for projects, which you can switch between easily.</li><li>Enabled OpenHarmony projects to support Ark build.</li><li>Moved the <strong id="b19966587296"><a name="b19966587296"></a><a name="b19966587296"></a>supportSystem "standard"</strong> field, which is exclusive to OpenHarmony projects, from the module-level <strong id="b596638162911"><a name="b596638162911"></a><a name="b596638162911"></a>build.gradle</strong> file to the project-level <strong id="b199671680295"><a name="b199671680295"></a><a name="b199671680295"></a>build.gradle</strong> file.</li></ul>
</div>
</td>
</tr>
</table>

