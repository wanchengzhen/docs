# 基于JS扩展的类Web开发范式



- **[概述](ui-js-overview.md)**

- **[框架说明](js-framework.md)**

- **[构建用户界面](ui-js-building-ui.md)**

- **[常见组件开发指导](ui-js-common-components.md)**

- **[动效开发指导](ui-js-animate.md)**

- **[自定义组件](ui-js-custom-components.md)**