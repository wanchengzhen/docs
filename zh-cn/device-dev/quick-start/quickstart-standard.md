# 标准系统入门<a name="ZH-CN_TOPIC_0000001111221726"></a>

-   **[标准系统入门简介](quickstart-standard-overview.md)**  

-   **[标准系统开发环境准备（仅Hi3516需要）](quickstart-standard-env-setup.md)**  

-   **[获取源码](quickstart-standard-sourcecode-acquire.md)**  

-   **[运行“Hello World”](quickstart-standard-running.md)**  

-   **[常见问题](quickstart-standard-faqs.md)**  

-   **[附录](quickstart-standard-appendix.md)**  


