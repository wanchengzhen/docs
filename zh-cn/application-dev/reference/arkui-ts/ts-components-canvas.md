# 画布组件

- **[Canvas](ts-components-canvas-canvas.md)**

- **[CanvasRenderingContext2D对象](ts-canvasrenderingcontext2d.md)**

- **[OffscreenCanvasRenderingConxt2D对象](ts-offscreencanvasrenderingcontext2d.md)**

- **[Lottie](ts-components-canvas-lottie.md)**

- **[Path2D对象](ts-components-canvas-path2d.md)**

- **[CanvasGradient对象](ts-components-canvas-canvasgradient.md)**

- **[ImageBitmap对象](ts-components-canvas-imagebitmap.md)**

- **[ImageData对象](ts-components-canvas-imagedata.md)**