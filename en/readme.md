# Overview

This project stores OpenHarmony documentation, including the quick start guide, development guides, and API reference. We appreciate your contribution to the OpenHarmony documentation.

## Contents

- [OpenHarmony Overview](OpenHarmony-Overview.md)
- Device development
    - Mini and Small System Development Guidelines \(Reference Memory < 128 MB\)
      - Device development
        - **overview**:  [Device Development Overview](device-dev/Readme-EN.md)
        - **quick-start**:  [Quick Start](device-dev/quick-start/Readme-EN.md)  \(covering environment setup, source code acquisition, build, and burning\)
        - Basic development capabilities
          - **Kernel**:  [Kernel for Mini Systems](device-dev/kernel/kernel-mini.md)
          - **Kernel：**[Kernel for Small Systems](device-dev/kernel/kernel-small.md)
          - **Drivers**:  [drivers](device-dev/driver/Readme-EN.md)
          - **Subsystems**:  [subsystems](device-dev/subsystems/Readme-EN.md)  \(such as compilation and building, graphics, DFX, and XTS\)
          - **Security**:  [privacy and security](device-dev/security/Readme-EN.md)
        - **guide**:  
          - [WLAN-connected Products](device-dev/guide/device-wlan.md)  \(LED peripheral control and third-party SDK integration\)
          - [Screenless Cameras](device-dev/guide/device-iotcamera-control.md)  \(camera control\)
          - [Cameras with a Screen](device-dev/guide/device-camera.md)  \(screen and camera control, visual application development\)
        - **porting**:
          - [Third-Party Library Porting Guide for Mini and Small Systems](device-dev/porting/porting-thirdparty.md) 
          - [Mini System SoC Porting Guide](device-dev/porting/porting-minichip.md)
          - [Small System SoC Porting Guide](device-dev/porting/porting-smallchip.md)
        - **bundles**: 
          - [HPM Bundle Development Specifications](device-dev/bundles/bundles-standard-rules.md)
          - [HPM Bundle Development Guidelines](device-dev/bundles/bundles-guide.md)
          - [HPM User Guide](device-dev/bundles/bundles-demo.md)
    - Standard System Development Guidelines \(Reference Memory ≥ 128 MB\)
      - Device development
        - **overview**:  [Device Development Overview](device-dev/Readme-EN.md)
        - **quick-start**:  [Quick Start](device-dev/quick-start/quickstart-standard.md)  \(covering environment setup, source code acquisition, build, and burning\)
        - Basic development capabilities
          - **Kernel**:  [Kernel for the Standard System](device-dev/kernel/kernel-standard.md)
          - **Drivers**:  [Drivers](device-dev/driver/Readme-EN.md)
          - **Subsystems**:  [Subsystems](device-dev/subsystems/Readme-EN.md)  \(such as compilation and building, graphics, DFX, and XTS\)
          - **Security**:  [Privacy and Security](device-dev/security/Readme-EN.md)
        - **guide**:
          - [Development Guidelines on Clock Apps](device-dev/guide/device-clock-guide.md)
          - [Development Example for Platform Drivers](device-dev/guide/device-driver-demo.md)
          - [Development Example for Peripheral Drivers](device-dev/guide/device-outerdriver-demo.md)
        - **porting**:  
          - [Standard  System SoC Porting Guide](device-dev/porting/standard-system-porting-guide.md)
          - [A Method for Rapidly Porting the OpenHarmony Linux Kernel ](device-dev/porting/porting-linux-kernel.md)
        - **bundles**: 
          - [HPM Bundle Development Specifications](device-dev/bundles/bundles-standard-rules.md)
          - [HPM Bundle Development Guidelines](device-dev/bundles/bundles-guide.md)
          - [HPM User Guide](device-dev/bundles/bundles-demo.md)
    - [FAQs](device-dev/faqs/Readme-EN.md)


-   App development
    -   **Overview**:  [Application Development Overview](application-dev/application-dev-guide.md)
    -   **quick-start**:  [Quick Start](application-dev/quick-start/Readme-EN.md)
    -   **ui**:  [UI](application-dev/ui/Readme-EN.md)
    -   **media**:  [Media](application-dev/media/Readme-EN.md)
    -   **security**: [Security](application-dev/security/Readme-EN.md)
    -   **connectivity**:  [Connectivity](application-dev/connectivity/Readme-EN.md)
    -   **Database**：[Data Management](application-dev/database/Readme-CN.md)
    -   **usb**: [USB Service](application-dev//usb/Readme-EN.md)
    -   **dfx**:  [DFX](application-dev/dfx/Readme-EN.md)
    -   **reference**:  [Development References](application-dev/reference/Readme-EN.md)
-   **glossary**:  [Glossary](device-dev/glossary/glossary.md)

## Version Change History

For details, see  [OpenHarmony Release Notes](release-notes/Readme.md).

## Third-Party Open-Source Software and License Notice

3rd-Party-License: [Third-Party Open-Source Software and License Notice](contribute/third-party-open-source-software-and-license-notice.md)


## How to Contribute

A great open-source project wouldn't be possible without the hard work of many contributors. We'd like to invite anyone from around the world to  [participate](contribute/contribution.md)  in this exciting journey, and we're grateful for your time, passion, and efforts!

You can evaluate available documents, make simple modifications, provide feedback on document quality, and contribute your original content. For details, see  [Documentation Contribution](contribute/documentation-contribution.md).

Excellent contributors will be awarded and the contributions will be publicized in the developer community.

