# Transition Animation<a name="EN-US_TOPIC_0000001193075094"></a>

-   **[Page Transition](ts-page-transition-animation.md)**  

-   **[Component Transition](ts-transition-animation-component.md)**  

-   **[Transition of Shared Elements](ts-transition-animation-shared-elements.md)**  


