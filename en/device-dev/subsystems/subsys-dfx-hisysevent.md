# DHiSysEvent Development<a name="EN-US_TOPIC_0000001195021448"></a>

-   **[HiSysEvent Logging Configuration](subsys-dfx-hisysevent-logging-config.md)**

-   **[HiSysEvent Logging](subsys-dfx-hisysevent-logging.md)**  

-   **[HiSysEvent Listening](subsys-dfx-hisysevent-listening.md)**  

-   **[HiSysEvent Query](subsys-dfx-hisysevent-querying.md)**  

-   **[HiSysEvent Tool Usage](subsys-dfx-hisysevent-tool.md)**  


