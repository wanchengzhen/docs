# JavaScript-based Web-Like Development Paradigm<a name="EN-US_TOPIC_0000001214730071"></a>

-   **[Overview](ui-js-overview.md)**  

-   **[Framework](js-framework.md)**  

-   **[Building the UI](ui-js-building-ui.md)**  

-   **[Common Component Development Guidelines](ui-js-common-components.md)**  

-   **[Animation Development Guidelines](ui-js-animate.md)**  

-   **[Custom Components](ui-js-custom-components.md)**  


