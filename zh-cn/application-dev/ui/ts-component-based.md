# 组件化

- **[@Component](ts-component-based-component.md)**

- **[@Entry](ts-component-based-entry.md)**

- **[@Preview](ts-component-based-preview.md)**

- **[@Builder](ts-component-based-builder.md)**

- **[@Extend](ts-component-based-extend.md)**

- **[@CustomDialog](ts-component-based-customdialog.md)**

