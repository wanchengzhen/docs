# 基础手势

- **[TapGesture](ts-basic-gestures-tapgesture.md)**

- **[LongPressGesture](ts-basic-gestures-longpressgesture.md)**

- **[PanGesture](ts-basic-gestures-pangesture.md)**

- **[PinchGesture](ts-basic-gestures-pinchgesture.md)**

- **[RotationGesture](ts-basic-gestures-rotationgesture.md)**

- **[SwipeGesture](ts-basic-gestures-swipegesture.md)**