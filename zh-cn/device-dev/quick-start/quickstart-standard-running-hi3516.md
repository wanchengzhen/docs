# Hi3516开发板<a name="ZH-CN_TOPIC_0000001188686298"></a>

-   **[创建应用程序](quickstart-standard-running-hi3516-create.md)**  

-   **[源码编译](quickstart-standard-running-hi3516-build.md)**  

-   **[镜像烧录](quickstart-standard-running-hi3516-burn.md)**  

-   **[镜像运行](quickstart-standard-running-hi3516-run.md)**  


