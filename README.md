# OpenHarmony Documentation

Welcome to the OpenHarmony documentation repository.

This repository stores device and application development documents provided by OpenHarmony. Your contribution to the OpenHarmony documentation will be highly appreciated.

## Contents

[Chinese Documentation](zh-cn/readme.md)

[English Documentation](en/readme.md)

## OpenHarmony Document Version Branches

### Latest Versions

 - master: the latest version.

 - OpenHarmony 3.1 Beta. [Learn more](en/release-notes/OpenHarmony-v3.1-beta.md)

 - OpenHarmony 3.0 LTS. [Learn more](en/release-notes/OpenHarmony-v3.0-LTS.md)

   This version is upgraded to OpenHarmony 3.0.1 LTS. [Learn more](en/release-notes/OpenHarmony-v3.0.1-LTS.md)

 - OpenHarmony v2.2 Beta2. [Learn more](en/release-notes/OpenHarmony-v2.2-beta2.md)

 - OpenHarmony 2.0 Canary. [Learn more](en/release-notes/OpenHarmony-2-0-Canary.md)

### Historical Stable Versions

OpenHarmony_v1.x_release: OpenHarmony v1.1.2 LTS. [Learn more](en/release-notes/OpenHarmony-v1.1.2-LTS.md)

[More versions](en/release-notes/)
