# TypeScript-based Declarative Development Paradigm<a name="EN-US_TOPIC_0000001169328728"></a>

-   **[Overview](ui-ts-overview.md)**  

-   **[Framework Overview](ts-framework.md)**  

-   **[Declarative Syntax](ts-declarative-syntax.md)**  

-   **[Experiencing the Declarative UI](ui-ts-experiencing-declarative-ui.md)**  

-   **[Defining Page Layout and Connection](ui-ts-page-layout-connections.md)**  


