# Kernel for Standard Systems

-   **[Linux Kernel Overview](kernel-standard-overview.md)**  

-   **[Guidelines for Using Patches on OpenHarmony Development Boards](kernel-standard-patch.md)**  

-   **[Guidelines for Compiling and Building the Linux Kernel](kernel-standard-build.md)**  


