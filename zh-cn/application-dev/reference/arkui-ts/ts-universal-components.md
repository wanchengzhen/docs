# 通用

- **[通用事件](ts-universal-events.md)**

- **[通用属性](ts-universal-attributes.md)**

- **[手势处理](ts-gesture-processing.md)**