# 安全

- 用户认证
  - [用户认证开发概述](userauth-overview.md)
  - [用户认证开发指导](userauth-guidelines.md)
- Hap包签名工具  
  - [Hap包签名工具开发指南](hapsigntool_guidelines.md)